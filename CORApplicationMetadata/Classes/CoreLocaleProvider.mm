#import <CORCommon/CORCommon.h>

extern "C"
{
    /*
     Returns device region and culture in format [region];[culture] which can be
     parsed by .NET to RegionInfo and CultureInfo.
     */
	const char* corGetDeviceRegionAndCulture() {

        NSString* iosData = [NSString stringWithFormat:@"%@;%@;%@",
                             [NSLocale currentLocale].countryCode,
                             [NSLocale currentLocale].localeIdentifier,
                             [NSLocale currentLocale].languageCode];
        
        NSLog(@"REGION DATA: %@",iosData);
        
        //const char* data = [iosData cStringUsingEncoding:NSUTF8StringEncoding];
        //char* ret = (char*) malloc(strlen(data) + 1);
        //strcpy(ret, data);
        
        return MakeStringCopy(iosData);;
	}
}
